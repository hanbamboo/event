# MedicineGod药神 v0.6

#### 介绍

MedicineGod（下述"**MG**"）此版本是基于**DevEco Studio**开发的HarmonyOS的APP。

使用MG可以存储自己的药品，通过简单的提示与图像管理自己拥有的药品，还可将药品信息分享给朋友（当线下交付药品后也可自己管理）。

已有功能：存储、显示、改动、删除

待增功能：分享、用户


#### 安装教程

1. 下载**DevEco Studio**

   https://developer.harmonyos.com/cn/develop/deveco-studio/
   
   **使用Devstudio 3.0.0.991 Beta4版本 运行正常**
   **compileSdkVersion ** `6`
2. 导入**MedicineGod**项目

3. 注册**华为账号**（已有跳至4）

4. 在线运行项目

5. （可略）对项目进行签名（已签名跳至6）

6. （可略）打包APK导出

#### 其他版本
1.请前往官网 medicinegod.cn 下载安卓版1.6
